# MongoDB

MongoDB là 1 mã nguồn mở, đa nền tảng, và phân phối database dựa trên tài liệu được thiết kế để dễ dàng phát triển ứng dụng và scaling. Là 1 cơ sở dữ liệu NoSQL được phát triển bởi MongoDB Inc.

Cơ sở dữ liệu MongoDB được xây dựng để lưu trữ 1 lượng lớn dữ liệu và hoạt động cũng nhanh chóng.

MongoDB không phải là 1 RDBMS. Nó gọi là cơ sở dữ liệu NoSQL. Nó đối lập với cơ sở dữ liệu SQL nơi nó không chuẩn hóa dữ liệu trong các lược đồ và bảng, trong đó mọi bảng đều có cấu trúc cố định. Thay vào đó, nó lưu trữ dữ liệu trên bộ sưu tập dưới dạng tài liệu dựa trên JSON và không thực thi các lược đồ. Nó không làm việc với bảng, hàng và cột như các cơ sở dữ liệu SQL khác.

VD:

<img src="./Images/mongodb-document.png"/>

## Ưu điểm của MongoDB

- MongoDB lưu trữ dữ liệu dưới dạng tài liệu dựa trên JSON không thực thi lược đồ. Nó cho phép chúng ta lưu trữ dữ liệu phân cấp trong 1 tài liệu. Điều này giúp dễ dàng lưu trữ và truy xuất dữ liệu 1 cách hiệu quả.
- Có thể dễ dàng tăng hoặc giảm quy mô theo yêu cầu vì nó là cơ sở dữ liệu dựa trên tài liệu. MongoDB cho phép ta chia nhỏ dữ liệu trên nhiều máy chủ.
- MongoDB cung cấp các chức năng phong phú như indexing, tổng hợp, lưu trữ tệp, ...
- MongoDB hoạt động nhanh chóng với dữ liệu khổng lồ
- MongoDB cung cấp trình điều khiển để lưu trữ và tìm nạp dữ liệu từ các ứng dụng khác nhau được phát triển bởi các công nghệ khác nhau như C#, Java,...
- MongoDB cung cấp các công cụ để quản lý cơ sở dữ liệu MongoDB.

## Tính năng MongoDB

- Mỗi cơ sở dữ liệu chứa collections lần lượt chứa các documents. Mỗi document có thể khác nhau với số fields khác nhau. Kích thước và nội dung của mỗi documnet có thể khác nhau.
- Cấu trúc documnet phù hợp hơn với cách các nhà phát triển xây dựng các class và object của họ bằng các ngôn ngữ lập trình tương ứng. Các nhà phát triển sẽ nói rằng các lớp của họ không phải row và column mà có cấu trúc rõ ràng với các key-value
- Các row (hoặc document) không cần phải có lược đồ được xác định nhất. Thay vào đó, các field có thể tạo nhanh chóng
- Mô hình dữ liệu có sẵn trong MongoDB cho phép bạn biểu diễn các mối quan hệ phân cấp, lưu trữ mảng và các cấu trúc phức tạp khác dễ dàng hơn.
- Khả năng mở rộng - Môi trường MongoDB có thể mở rộng. Các cty trên thế giới đã xác định các cluster với 1 số trong số họ chạy hơn 100 node với khoảng hàng triệu document trong cơ sở dữ liệu

## Các thành phần chính trong MongoDB

<img src="./Images/mongodb-document-2.png"/>

1. Trường `_id` : Là trường bắt buộc trong documnet, đại diện cho 1 giá trị duy nhất trong document. Nó giống như primary key của document.
2. `collection` : Là 1 nhóm các document MongoDB. Một collection tương đương với 1 table được tạo bất kỳ trong RDMS. 1 collection tồn tại trong 1 cơ sở dữ liệu duy nhất. 
3. `Cursor` : Là 1 con trỏ đến tập kết quả của 1 truy vấn. Khách hàng có thể lặp lại qua 1 con trỏ để truy xuất kết quả.
4. `Database` : Là 1 thùng chứa collection như trong RDMS trong đó nó là vùng chứa cho tables. Mỗi cơ sở dữ liệu có 1 bộ tệp riêng trên hệ thống tệp. 1 server MongoDB có thể lưu trữ nhiều cơ sở dữ liệu
5. `Document` : 1 bản ghi trong 1 collection MongoDB được gọi là document. Document gồm tên field và values.
6. `Field` : 1 cặp name-value trong document. 1 document có 0 hoặc nhiều field. Các field tương tự như 1 column trong cơ sở dữ liệu. 
7. `JSON` : Được gọi là Object JavaScript.

