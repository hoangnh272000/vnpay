# Load Balancer

Load Balancer là 1 thành phần quan trọng của cơ sở hạ tầng thường đc sử dụng để cải thiện hiệu suất và độ tin cậy của các trang web, các ứng dụng, cơ sở dữ liệu và các dịch vụ khác bằng cách phân phối khối lượng công việc trên nhiều server.

1 cơ sở hạ tầng web không có load balancer:

<img src="./Images/no-load-balancer.png"/>

Load balancer hoạt động như 'traffic cop' trước server của bạn và định tuyến các yêu cầu của khách hàng trên tất cả server có khả năng đáp ứng các yêu cầu đó theo các tối ưu hóa tốc độ và việc sử dụng lưu lượng và không có server nào làm việc quá sức, điều này có thể làm giảm hiệu xuất. Nếu 1 server gặp sự cố, load balancer sẽ chuyển hướng lưu lượng truy cập tới các server trực tuyến còn lại. Load balancer sẽ thực hiện các chức năng sau:
- Phân phối các yêu cầu của máy khách hoặc tải mạng 1 cách hiệu quả trên nhiều server.
- Đảm bảo tính khả dụng và độ tin cậy cao bằng cách chỉ gửi yêu cầu đến các server trực tuyến.
- Cung cấp linh hoạt thêm hoặc bớt các server khi nhu cầu ra lệnh.

<img src="./Images/load-balancer.png"/>

## Load balancer chọn backend server

Load balancer chọn server nào để chuyển tiếp request dựa trên 2 yêu cầu. 

### Health Checks

Load balancer chỉ chuyển tiếp đến lưu lượng tới "healthy" backend servers. Để theo dõi health của 1 backend server, health checks thường xuyên cố gắng kết nối với các backend server bằng protocol và port được xác định bởi các quy tắc chuyển tiếp để đảm bảo các server đang listen. Nếu 1 server lỗi health check, do đó không thể thực hiện các yêu cầu, nó sẽ tự động bị xóa khỏi nhóm, và các lưu lượng sẽ không được chuyển tiếp đến nó cho đến khi nó phản hồi lại health check.

### Load balancer algorithms (thuật toán load balancer)

Các algorithms khác nhau cung cấp các lợi ích khác nhau; việc lựa chọn load balance algorithms phụ thuộc vào nhu cầu của bạn:
- **Round Robin**: Các yêu cầu được phân phối tuần tự trên group servers.
- **Least Connections**: Yêu cầu mới được gửi tới server có ít kết nối nhất đến clients tại thời điểm hiện tại
- **Least Time**: Gửi yêu cầu đến server theo công thức kết hợp thời gian phản hồi nhanh nhất và ít kết nối hoạt động nhất (dành riêng cho NGINX Plus)
- **Hash**: Phân phối các yêu cầu trên 1 khóa xác định. VD: client IP address hoặc URL yêu cầu.
- **IP Hash**: IP address của client được sử dụng để xác định server nào nhận được yêu cầu
- **Random with Two Choices**: Chọn ngẫn nhiên 2 server và gửi yêu cầu đến 1 cái được chọn sau đó áp dụng algorithms **Least Time**.

1. Round robin 

Mặc định thì `nginx` sẽ sử dụng thuật toán này để tính lưu lượng cho các server con. Với thuật toán này các server sẽ được lần lượt nhận các request theo thứ tự cụ thể.
- Request 1 ==> server_1
- Request 2 ==> server_2
- .......................
- Request n ==> server_1
- Request n+1 ==> server_2
```
upstream all {
    server server_1;
    server server_2;
}
```

2. Least Connections

Nginx sẽ tính lưu lượng trafic và connection thực tế mà server đó phải đang xử lý và sẽ phân bổ request mới đến các server đang phải xử lý ít request hơn.
```
upstream all {
    least_conn;
    server server_1;
    server server_2;
}
```

3. Hash

- Ta cần cung cấp 1 `key` cho nginx, `key` này có thể là text cũng có thể là argument, header hay cookie,... của request truyền lên.
- Nginx sử dụng `key` này để mã hóa và check xem `key` này đã được gán với server nào (trong cache)
- Nếu chưa gán với server nào thì nginx sẽ random server(theo Round robin) và cache lại
- Nếu đã gán server thì auto chọn server đó.
```
hash 'key' [consistent]
```
key:
- Header : `$http_session_id` ==> Lấy Header có name là session_id
- Argument : `$arg_mobile` ==> Lấy Argument có name là mobile
- Ip address : `$remote_addr`
```
upstream all {
    hash $http_session_id consistent; #use method ketama
    server server_1;
    server server_2;
}
```

4. ip_hash

Thuật toán cũng sử dụng hash và nó là hash ip address của client. Bản chất của nó sẽ sử dụng 3 octet của ipv4 hoặc toàn bộ địa chỉ ipv6 để làm `key` mã hóa rồi hoạt động như thuật toán `hash` thông thường. Thuật toán này đảm bảo rằng, khi hoạt động bình thường, request từ 1 client luôn luôn chỉ tới 1 server.
```
upstream all {
    ip_hash;
    server server_1;
    server server_2;
}
```

5. Least Time (Dành cho phiên bản trả phí Nginx Plus)

Việc quyết định sử dụng server nào phụ thuộc vào lượng request active của server và độ trễ khi xử lý của server. Nó sẽ chọn server ít connection và độ trễ thấp nhất. Việc tính độ trễ này phụ thuộc vào tham số đầu vào của `least_time`.
- connect ==> Thời gian connecg đến upstream server.
- first_byte ==> Thời gian nhận về những byte dữ liệu đầu tiên
- last_byte ==> Thời gian nhận hết toàn bộ dữ liệu
```
upstream all{
    least_time first_byte;
    server server_1;
    server server_2;
}
```

6. Random with Two Choices

Random các server để lựa chọn server mà request sẽ đi tới. 
```
upstream all {
    random two least_conn;
    server server_1;
    server server_2;
    server server_3;
}
```
