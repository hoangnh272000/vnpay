# LVM

LVM - Logical Volume Manager : là 1 công cụ để quản lý khối lượng hợp lý bao gồm phân bổ disks, striping, mirroring và resizing Logical Volume 

Với LVM, 1 ổ đĩa cứng hoặc bộ đĩa cứng phân bổ cho 1 hoặc nhiều physical volumes. LVM physical volume có thể đặt trên các thiết bị chặn khác có thể kéo dài thành 2 hoặc nhiều ổ đĩa

Các physical volumes kết hợp thành logical volumes, ngoài trừ `/boot`. /boot không ở trong logical volume group bởi vì boot không thể đọc nó.

Vì 1 physical volume không thể trải dài trên nhiều ổ đĩa, để kéo dài trên nhiều ổ đĩa, tạo 1 hoặc nhiều physical volumes trên mỗi ổ đĩa.

<img src="./Images/lvg.png"/>

Các volume group có thể được chia thành nhiều logical volume

## Cấu hình LVM

LVM có thể cấu hình trong quá trình cài đặt đồ họa. Bạn có thể dùng `system-config-lvm` để tạo cấu hình LVM riêng của bạn sau khi cài đặt. Tổng quan các bước cần thiết để cấu hình LVM bao gồm:
- Tạo physical volumes từ các ổ cứng
- Tạo volume groups từ physical volumes
- Tạo logical volumes từ volume groups và chỉ định các điểm gắn kết logical volumes

<img src="./Images/lvm.png"/>

## Install LVM

```
sudo yum makecache
sudo yum install lvm2
```

### Khởi tạo disk cho LVM

```
sudo fdisk /dev/sdb
```

<img src="./Images/fdisk-sdb.png"/>

Kiểm tra ổ đĩa trong máy
```
lsblk
```

Thêm disk vào LVM PV:
```
sudo pvcreate /dev/sbd1
```

Có thể xem danh sách PV
```
sudo pvscan
```

Xem thông tin của những PV
```
sudo pvdisplay /dev/sdb1
```

### Tạo Volume Group

```
sudo vgcreate share /dev/sdb1
```

Kiểm tra danh sách VG
```
sudo vgscan
```

Xem thông tin chi tiết của VG
```
sudo vgdisplay share
```

### Mở rộng Volume Group

Có thể thêm nhiều PV vào VG share
```
sudo vgextend share /dev/sdb2
```

### Tạo Logical Volumes

Có thể tạo bao nhiều LV bằng cách sử dụng VG.

Tạo 1 LV 100MB www-hoang từ VG share
```
sudo lvcreate --size 100M --name www-hoang share
```

Kiểm tra danh sách LV
```
sudo lvscan
```
hoặc
```
sudo lvs
```

Xem thông tin chi tiết của LV
```
sudo lvdisplay VG_NAME/LV_MANE
```

### Định dạng và gắn kết Logical Volume

Có thể truy cập LV như cách truy cập thông thường /dev/sda

Các LV có sẵn dưới dạng **/dev/VG_NAME/LV_NAME**

Câu lệnh để định dạng /dev/share/www-hoang sang hệ thống tệp ext4
```
sudo mkfs.ext4 /dev/share/www-hoang
```

Gắn kết LV /dev/share/www-hoang:
```
sudo mkdir -pv /var/www/hoang
```

Mount /dev/share/www-hoang vào bất kỳ thư mục trống nào như /var/www/hoang
```
sudo mount /dev/share/www-hoang /var/www/hoang
```

### Mở rộng Logical Volumes

Có thể thay đổi kích thướng LV 1 cách nhanh chóng

Thêm 500MB vào LV www-hoang từ VG share:
```
sudo lvextend --size +500MB --resizefs share/www-hoang
```

Sau khi reboot máy tính file mount sẽ bị mất đi, để điều này không xảy ra, ta sẽ setting trong thư mục `/etc/fstab`
```
vi /etc/fstab
```

Sửa lại trong file

<img src="./Images/fstab.png"/>

Lấy UUID của LV đã tạo ra bằng câu lệnh
```
blkid
```

<img src="./Images/cmd-blkid.png"/>

