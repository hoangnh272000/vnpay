# Tuning 

# Hardening

# Openscap

Với OpenSCAP, bạn có thể kiểm tra cài đặt cấu hình bảo mật của hệ thống, và kiểm tra hệ thống để tìm các dấu hiệu của sự thỏa hiệp bằng cách sử dụng các quy tắc tiêu chuẩn và thông số.

Tập hợp các mã nguồn mở để triển khai và tuân thủ tiêu chuẩn SCAP (Giao thức tự động hóa nội dung bảo mật) được chứng nhận bởi NIST (Viện tiêu chuẩn và công nghệ quốc gia).

SCAP được tạo ra để cung cấp 1 cách cung cấp tiêu chuẩn hóa để duy trì bảo mật hệ thống

## Cài đặt OpenSCAP

```
sudo apt install libopenscap8
```

Sau khi cài đặt xong có thể sử dụng cmd `oscap` để kiểm tra
```
oscap --version
```

## Nhận SCAP content

Để thực hiện các tác vụ với OpenSCAP, bạn cần có chính sách bảo mật ở dạng SCAP. Có nhiều nhà cung cấp SCAP content
```
sudo apt install scap-security-guide
```

SCAP content được cài đặt tại thư mục /usr/share/xml/scap/ssg/content

## Hiển thị thông tin SCAP content

Sử dụng command `oscap info`