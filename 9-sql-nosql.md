# SQL - Structured Query language

Là ngôn ngữ chuẩn để xử lý cơ sở dữ liệu quan hệ. 1 cơ sở dữ liệu quan hệ xác định các mối quan hệ dưới dạng các bảng

Lập trình SQL có thể sử dụng hiệu quả để chèn, tìm kiếm, cập nhật, xóa các bản ghi cơ sở dữ liệu

# NoSQL 

Là 1 DMS không quan hệ, không yêu cầu 1 lược đồ cố định, tránh các phép nối, và dễ dàng mở rộng. Cơ sở dữ liệu NoSQL được sử dụng cho các kho dữ liệu phân tán với nhu cầu lưu trữ dữ liệu khổng lồ. NoSQL được sử dụng cho dữ liệu lớn và các ứng dụng web thời gian thực. Ví dụ như các công ty như Twitter, Facebook, Google thu thập hàng terabyte dữ liệu người dùng mỗi ngày.

## Khi nào sử dụng SQL

- SQL là ngôn ngữ đơn giản nhất được sử dụng để giao tiếp với RDBMS
- Phân tích các phiên liên quan đến hành vi và tùy chỉnh
- Tạo trang tổng quan tùy chỉnh
- Nó cho phép bạn lưu trữ và lấy dữ liệu từ cơ sở dữ liệu 1 cách nhanh chóng
- Được ưu tiên khi bạn muốn sử dụng các phép nối và thực hiện các truy vấn phức tạp

## Khi nào sử dụng NoSQL

- Khi không cần hỗ trợ ACID
- Khi mô hình RDBMS truyền thống không đủ
- Dữ liệu cần lược đồ linh hoạt
- Các ràng buộc và logic xác thực không bắt buộc phải được thực hiện trong cơ sở dữ liệu 
- Ghi nhật ký từ các nguồn được phân phối
- Nó nên được sử dụng để lưu dữ liệu tạm thời như giỏ mua hàng, danh sách mong muốn và dữ liệu phiên.

