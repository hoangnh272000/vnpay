# Firewall

Là 1 thiết bị bảo mật mạng giám sát lưu lượng mạng đến và đi và cho phép hoặc chặn các gói dữ liệu dựa trên 1 tập hợp các quy tắc bảo mật. Mục đích của nó là thiết lập 1 rào cản giữa mạng nội bộ của bạn và lưu lượng đến từ các nguồn bên ngoài để chặn lưu lượng độc hại như virut và tin tặc.

## Firewall làm việc thế nào?

Firewall phân tích cẩn thận lưu lượng đến dựa trên các quy tắc được thiết lập trước và lọc lưu lượng truy cập đến từ các nguồn không an toàn hoặc đáng ngờ để ngăn chặn các cuộc tấn công. Firewall bảo vệ tại điểm đầu vào của máy tính, gọi là `port`, nơi thông tin được trao đổi với thiết bị bên ngoài.

<img src="./Images/firewall.png"/>

Nếu laptop của bạn không có firewall, khi kết nối internet, tất cả các giao thông ra vào mạng đều được cho phép, vì thế hacker, virus có thể truy cập và lấy cắp thông tin của bạn trên máy tính.

## Phân loại firewall

Có 2 loại firewall: firewall cứng và firewall mềm

## Firewall cứng

Là 1 phần thiết bị được cài đặt giữa network và gateway của bạn.

<img src="./Images/firewall-cung.png"/>

Đặc điểm:
- Không được linh hoạt như firewall mềm: Không thể thêm chức năng, thêm quy tắc như firewall mềm
- Firewall cứng hoạt động ở tầng thấp hơn firewall mềm (Tầng network và tầng transport)
- Firewall cứng không thể kiểm tra nội dung của một gói tin
- 1 số firewall cứng thông dụng: NAT, Cisco ASA 5500,...

## Firewall mềm

Là 1 chương trình được cài đặt trên mỗi máy tính và điều chỉnh lưu lượng truy cập thông qua port và ứng dụng.

<img src="./Images/firewall-mem.png"/>

Đặc điểm:
- Tính linh hoạt cao: có thể thêm, bớt các quy tắc, các chức năng
- Firewall mềm hoạt động ở tầng cao hơn firewall cứng (tâng application)
- Firewall mềm có thể kiểm tra được nội dung của gói tin(thông qua các từ khóa)
- Các firewall mềm phổ biến: Zone alarm, norton firewall,...

## Packet-filtering firewall

Packet-filtering firewall, loại tường lửa phổ biến nhất, kiểm tra các gói và cấm chúng đi qua nếu chúng không khớp với toàn bộ quy tắc bảo mật đã thiết lập. Loại firewall này kiểm tra địa chỉ IP nguồn và đích của gói. Nếu các gói phù hợp với quy tắc "allowed" trên firewall, thì nó được tin cậy để vào mạng.

Packet-filtering firewall được chia thành 2 loại : stateful và stateless. Stateless firewalls kiểm tra các gói độc lập với nhau và thiếu ngữ cảnh, biến chúng thành mục tiêu dễ dàng cho hackers. Statefull firewall ghi nhớ thông tin về các gói đã truyền trước đó và được coi là an toàn hơn nhiều.

Mặc dù packet-filtering firewall có thể khá hiệu quả, chúng cung cấp sự bảo vệ cơ bản

## Next-generation firewalls (NGFW)

Sự kết hợp công nghệ firewall truyền thống với chức năng bổ sung, chẳng hạn như kiểm tra lưu lượng được mã hóa, hệ thống ngăn chặn xâm nhập, chống virut,... Điểm đáng chú ý, nó có kiểm tra gói tin sâu(DPI). Trong khi firewall cơ bản chỉ kiểm tra tiêu đề gói, kiểm tra sâu sẽ kiểm tra dữ liệu bên trong chính gói đó, cho phép người dùng xác định, phân loại hoặc ngăn chặn các gói dữ liệu độc hại 1 cách hiệu quả hơn.

## Proxy firewall

Lọc lưu lượng mạng ở cấp ứng dụng. Proxy đóng vai trò trung gian giữa 2 hệ thống đầu cuối. Client phải gửi 1 request đến firewall, nơi nó được đánh giá dựa trên 1 bộ quy tắc bảo mật và sau đó được phép hoặc bị mở rộng. Đáng chú ý, proxy firewalls giám sát lưu lượng truy cập cho các giao thức lớp 7 như HTTP và FTP, đồng thời sử dụng cả kiểm tra gói trạng thái để phát hiện lưu lượng độc hại.

## Network address translation (NAT) firewalls 

Cho phép nhiều thiết bị có địa chỉ mạng độc lập kết nối với internet bằng 1 địa chỉ IP duy nhất, ẩn các địa chỉ IP riêng lẻ. Do đó, những kẻ tấn công đang quét mạng để tìm địa chỉ IP không thể nắm bắt được các chi tiết cụ thể, mang lại khả năng bảo mật cao hơn trước các cuộc tấn công.

## Stateful multilayer inspection (SMLI) firewalls 

Lọc các gói ở lớp network, lớp transport và lớp application, so sáng chúng với các gói tin cậy đã biết. Giống như NGFW, SMLI cũng kiểm tra toàn bộ gói và chỉ cho phép chúng vượt qua từng layer riêng lẻ

# SElinux

Security-Enhanced Linux (SElinux) là 1 kiến trúc bảo mật cho hệ thống linux cho phép administrators có quyền điều khiển nhiều hơn người dùng khác.

## SElinux hoạt động như thế nào

Xác định các điều khiển truy cập cho ứng dụng, quy trình và các file trên hệ thống. 

Nếu SElinux không thể đưa ra quyết định về quyền truy cập dựa trên các quyền đã lưu trong cached, nó sẽ gửi yêu cầu tới security server. Security server kiểm tra security context của app hoặc quy trình của file

Security context áp dụng từ SElinux policy database. Quyền sau đó được cấp hoặc từ chối. Nếu quyền bị từ chối ta có thể xem ở /var/log.messages.

## Cách cấu hình SElinux

Có 1 số cách để cấu hình SElinux để bảo vệ hệ thống của bạn. Phổ biến nhất là chính sách nhắm mục tiêu (targeted policy) hoặc bảo vệ đa cấp (multi-level security - MLS)

Targeted policy là lựa chọn mặc định và bao gồm 1 loạt các quy trình, tasks và services. MLS rất phức tạp và thường được sử dụng bởi các tổ chức chính phủ

Có thể xem hệ thống đang chạy ở file /etc/sysconfig/selinux

## SElinux labeling and kiểu thực thi

SElinux làm việc giống như 1 hệ thống ghi nhãn, tức là tất cả các trường, quy trình và ports trong hệ thống có SElinux label gán với nó. Labels là cách hợp lý để nhóm mọi thứ lại vs nhau. Kernel quản lý label trong quá trình khởi động

Labels có mẫu như user: role:type:level

## Enabling SElinux

Nếu SElinux bị tắt, ta có thể vào file /etc/selinux/config 
- SELINUX=disabled - tắt hoàn toàn
- SELINUX=permissive - chế độ chỉ ghi ra log các yêu cầu truy cập, luôn cấp quyền truy cập
- SELINUX=enforcing - kiểm tra quyền đầy đủ