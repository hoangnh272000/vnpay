# Webserver

Webserver có thể là phần cứng hoặc phần mềm, hoặc cả 2 phần cùng làm việc với nhau. Được kết nối và liên kết mạng máy tính mở rộng. Webserver được cài đặt các chương trình để phục vụ ứng dụng web, chứa toàn bộ dữ liệu và nắm quyền quản lý. Webserver có thể lấy thông tin request từ duyệt trình web và gửi phản hồi tới máy khách thông qua HTTP hoặc giao thức khác.

- Phần cứng : 1 web server là 1 máy tính lưu trữ phần mềm web server và các file thành phần của website (VD: HTML Doc, images, CSS, files JavaScript). Một web server kết nối tới internet và hỗ trợ trao đổi dữ liệu vật lý với các thiết bị khác được kết nối với web.
- Phần mềm : 1 web server bao gồm 1 số phần kiểm soát cách người dùng web truy cập các tệp được lưu trữ. Ở mức tối thiểu, đây là 1 HTTP server. 1 HTTP server là phần mềm hiểu URL và HTTP. HTTP server có thể được truy cập thông qua tên miền của các trang web mà nó lưu trữ và nó cung cấp nội dung của các trang web mà nó lưu trữ này đến thiết bị của người dùng.

Để công khai 1 trang web thì ta cần 1 web server tĩnh hoặc động.
- Web server tĩnh: Bao gồm 1 máy tính với 1 HTTP server. Gọi là "tĩnh" vì server gửi các tệp được lưu trữ trên duyệt trình của bạn.
- Web server động: Bao gồm 1 web server dộng và phần mềm kèm theo, phổ biến nhất là 1 server và 1 database. Gọi là "động" vì application server cập nhật các file được lưu trữ trước khi gửi nội dung đến duyệt trình của bạn thông qua HTTP server.

## Deeper dive

Để tìm 1 webpage, duyệt trình của bạn gửi 1 yêu cầu tới webserver, webserver sẽ xử lý và tìm kiếm file được yêu cầu đó trong bộ nhớ webserver. Trong việc tìm file, Server sẽ đọc file đó xử lý nó nếu cần rồi gửi về cho duyệt trình

### Hosting files

Đầu tiên, webserver lưu trữ các file của trang web, nó là các file HTML và các file liên quan đến nó như image, CSS, files JavaScript, video,...

Về mặt kỹ thuật, bạn có thể lưu trữ các file đó trên máy tính, nhưng sẽ thuận tiện hơn nếu bạn lưu trữ nó trên webserver:
- Luôn chạy và chạy
- Không bị downtime và sự cố hệ thống, 1 webserver chuyên dụng luôn kết nối với internet
- Luôn cùng IP address.
- Luôn được duy trì bởi 1 bên thứ 3

### Communicating through HTTP (Giao tiếp thông qua HTTP)

Thứ 2, webserver cung cấp hỗ trợ HTTP.

Protocol là 1 tập hợp các quy tắc để giao tiếp giữa 2 máy tính. HTTP là 1 dạng văn bản, stateless protocol.
- Textual (văn bản) : Tất cả lệnh đều là văn bản thuần thúy và con người có thể đọc được
- Stateless : Cả server và client đều không nhớ các thao tác trước đó.

HTTP cung cấp các quy tắc rõ ràng về cách giao tiếp của client và server. Chú ý những điều sau:
- Chỉ client thực hiện yêu cầu HTTP cho server. Server phản hồi yêu cầu HTTP của client. 
- Khi yêu cầu 1 file qua HTTP, client phải cung cấp URL của file
- Webserver phải trả lời mọi yêu cầu HTTP, ít nhất với 1 thông báo lỗi.

Trên webserver, HTTP server chịu trách nhiệm trả lời và xử lý các yêu cầu đến.
- Khi nhận yêu cầu, HTTP server sẽ kiểm tra URL có khớp với file hiện có không
- Nếu có, webserver sẽ gửi nội dung file trở lại duyệt trình. Nếu không, server ứng dụng sẽ xây dựng 1 file cần thiết.
- Nếu không xử lý được, webserver trả lại thông báo lỗi cho duyệt trình, thường là "404 not found".

# NGINX

## Nginx là gì?

Là 1 webserver mã nguồn mở phục vụ web HTTP. Nó có thể phục vụ các công việc như: Load balancing, HTTP caching, media streaming và email proxy,...

<img src="./Images/nginx.png"/>

## Cách thức hoạt động của nginx

Các luồng được quản lý trong mỗi tiến trình và mỗi tiến trình (process) chứa các đơn vị nhỏ hơn là worker connection và cả bộ worker connection chịu trách nghiệm xử lý các threads cung cấp các yêu cầu của work process và sẽ gửi đến master process. Và cuối cùng master process trả kết quả cho những yêu cầu đó

## Cấu trúc của file cấu hình

Nginx bao gồm các module được điều khiển bởi các chỉ thị được chỉ định trong file cấu hình. Các chỉ thị được chia thành simple và block. 1 chỉ thị simple bao gồm tên và các tham số được phân tách = dấu cách và kết thúc bằng ';'. 1 chỉ thị block tương tự chỉ thị simple chỉ khác là kết thúc bằng {}. Bên trong chỉ thị block có thể có nhiều chỉ thị khác bên trong {}, nó gọi là context (Vd: events, http, server, location)

Các chỉ thị được đặt trong file cấu hình bên ngoài của bất kỳ context nào được coi là bên trong main context. Chỉ thị `event`, `http` nằm bên trong `main` context, `server` bên trong `http`, `location` ở trong `server`.

## Cung cấp nội dung tĩnh

