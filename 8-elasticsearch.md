# Elasticsearch

là 1 công cụ phân tích và tìm kiếm RESful phân tán được thiết kế để đáp ứng nhiều trường hợp sử dụng. Nó tập trung hóa lưu trữ dữ liệu của bạn và đảm bảo nghiên cứu cực nhanh, mức độ liên quan rất cao và các phân tích mạnh mẽ vì chúng có thể mở rộng.

- Elasticsearch là 1 search engine
- Elasticsearch được kế thừa từ Lucene apache
- Elasticsearch thực chất hoạt động như 1 web server, có khả năng tìm kiếm nhanh chóng.
- Elasticsearch có khả năng phân tích và thống kê dữ liệu
- Elasticsearch chạy trên server riêng và đồng thời giao tiếp thông qua RESTful do vậy nên nó không phụ thuộc vào client viết bằng gì hay hệ thống hiện tại của bạn viết bằng gì. Nên việc tích hợp nó vào hệ thống bạn là dễ dàng, bạn chỉ cần gửi request http lên là nó trả về kết quả.
- Elasticsearch là 1 hệ thống phân tán và có khả năng mở rộng tuyệt vời.
- Elasticsearch là opensource được phát triển bởi java

## Khái niệm cơ bản

### Document

Là 1 JSON object với 1 số dữ liệu. Đây là basic information unit trong ES, là đơn vị nhỏ nhất để lưu trữ dữ liệu trong Elasticsearch.

### Index

Trong Elasticsearch, sử dụng 1 cấu trúc được gọi là inverted index. Nó được thiết kế để cho phép tìm kiếm `full-text search`. Các văn bản được phân tách thành từng từ có nghĩa sau đó sẽ được map xem thuộc văn bản nào 

### Shard

- Shard là đối tượng của Lucene, là tập con các documents của 1 index. 1 index có thể chia thành nhiều shard
- Mỗi node bao gồm nhiều shard. Chính vì thế shard là đối tượng nhỏ nhất, hoạt động ở mức thấp nhất, đóng vai trò lưu dữ liệu.
- Chúng ta gần như không bao giờ làm việc trực tiếp với các shard vì Elasticsearch đã support toàn bộ việc giao tiếp cũng như tự động thay đổi shard khi cần thiết.
- Có 2 loại shard là : primary shard và replica shard.

#### Primary shard

- Primary shard sẽ lưu dữ liệu và đánh index. Sau khi đánh xong dữ liệu sẽ được vận chuyển tới các replica shard
- Mặc định mỗi index sẽ có 5 primary shard và mỗi primary shard sẽ ứng với 1 replica shard.

#### Replica shard

- Replica shard là nơi lưu trữ dữ liệu nhân bản của primary shard
- Replica shard có vai trò đảm bảo tính toàn vẹn của dữ liệu khi Primary shard xảy ra vấn đề.
- Ngoài ra replica shard có thể giúp tăng cường tốc độ tìm kiếm vì chúng ta có thể setup lượng replica shard nhiều hơn mặc định của ES.

### Node

- Là trung tâm hoạt động của Elasticsearch. Là nơi lưu trữ dữ liệu, tham gia thực hiện đánh index của cluster cũng như thực hiện các thao tác tìm kiếm
- Mỗi node được định danh bằng **unique name**

### Cluster 

- Tập hơn các nodes hoạt động cùng với nhau, chia sẻ cùng thuộc tính `cluster.name`. Cluster được xác định bằng 1 'unique name'. Việc định danh các cluster trùng tên sẽ gây lỗi cho các node.
- Mỗi cluster có 1 node chính (master), được lựa chọn tự động và có thể thay thế nếu sự cố xảy r. 1 cluster có thể gồm 1 hoặc nhiều node. Các node hoạt động trên 1 server. 
- Chức năng chính của cluster là quyết định xem shards nào được phân bổ cho node nào và khi nào thì di chuyển các cluster để cân bằng lại cluster.

## Ưu điểm

- TÌm kiếm dữ liệu rất nhanh chóng, mạnh mẽ dựa trên apache lucene
- Có khả năng phân tích dữ liệu
- Khả năng mởi rộng theo chiều ngang tuyệt vời
- Hỗ trợ tìm kiếm mờ (fuzzy), tức là từ khóa tìm kiếm có thể bị sai lỗi chính tả hay không đúng cú pháp thì vẫn có khả năng elasticsearch trả về kết quả tốt.
- Hỗ trợ nhiều Elasticsearc client như Java, PhP, Javascript, Ruby, .NET, Python

## Nhược điểm 

- Elasticsearch được thiết kế cho mục đích search, do vậy với những nhiệm vụ khác ngoài search như CRUD thì elastic kém thế hơn so với những database khác như Mongodb, Mysql …. Do vậy người ta ít khi dùng elasticsearch làm database chính, mà thường kết hợp nó với 1 database khác
- Trong elasticsearch không có khái niệm database transaction , tức là nó sẽ không đảm bảo được toàn vẹn dữ liệu trong các hoạt độngInsert, Update, Delete.Tức khi chúng ta thực hiện thay đổi nhiều bản ghi nếu xảy ra lỗi thì sẽ làm cho logic của mình bị sai hay dẫn tới mất mát dữ liệu. Đây cũng là 1 phần khiến elasticsearch không nên là database chính
- Không thích hợp với những hệ thống thường xuyên cập nhật dữ liệu. Sẽ rất tốn kém cho việc đánh index dữ liệu.

# ELK

Là viết tắt của 3 dự án mã nguồn mở: Elasticsearch, Logstash, Kibana. `Elasticsearch` là 1 công cụ phân tích và tìm kiếm. `Logstash` là 1 server-side pipeline, dành cho xử lý dữ liệu(loại ETL). Nhiệm vụ của `logstash` là nhập dữ liệu đồng thời từ nhiều nguồn, sau đó chuyển đổi dữ liệu đó và gửi dữ liệu đó đến hệ thống lưu trữ như `Elasticsearch`.