# Logstash

1 ứng dụng rất phổ biến để thu thập, xử lý và lọc log data.

Logstash là 1 mã nguồn mở, pipeline xử lý dữ liệu phía máy chủ có thể đồng thời nhập dữ liệu từ nhiều nguồn khác nhau, sau đó phân tích cú pháp, lọc, biến đổi và làm phong phó dữ liệu, và cuối cùng chuyển tiếp nó đến 1 downstream system.

Trong hầu hết các trường hợp, downstream system là Elasticsearch

Logstash thường được sử dụng làm công cụ "xử lý" cho bất kỳ log management nào(hoặc các hệ thống xử lý các stream dữ liệu thay đổi).

Logstash có 3 phần chính cũng chính là 3 bước xử lý chính của logstash đó:
- INPUT : Nó có thể lấy đầu vào từ TCP/UDP, các file, từ syslog, STDIN và nhiều nguồn khác. Chúng ta có thể lấy log từ các ứng dụng trên môi trường của chúng ta rồi đẩy chúng tới logstash.
- FILTER : Khi những log này tới Server Logstash, có 1 lượng lớn các bộ lọc mà cho phép chúng ta có thể chỉnh sửa và chuyển đổi những event này. Ta có thể lấy ra các thông tin mà ta cần từ những event log.
- OUTPUT : Khi xuất dữ liệu ra, Logstash hỗ trợ rất nhiều các đích tới bao gồm TCP/UDP, email, các file, HTTP, Nagios và số lượng lớn các dịch vụ mạng. Ta có thể tích hợp Logstash với các công cụ tính toán số liệu (metric), các công cụ cảnh báo, các dạng biểu đồ, các công nghệ lưu trữ hay ta có thể xây dựng 1 công cụ trong môi trường làm việc của chúng ta.

## Logstash hoạt động như thế nào?

<img src="./Images/logstash.png"/>

Dữ liệu chảy qua 1 pipeline Logstash trong 3 giai đoạn: Giai đoạn input, giai đoạn filter và giai đoạn output. Trong mỗi giai đoạn, các plugins thực hiện 1 số hành động trên dữ liệu

Trong ELK stack, công cụ lưu trữ là Elasticsearch, UI là Kibana. (thông thường)

### Input

<img src="./Images/logstash-input.png"/>

Chúng ta sử dụng input để lấy dữ liệu vào Logstash. Một số đầu vào thường đc sử dụng là:
- File : Đọc từ 1 tệp trên hệ thống, giống như lệnh UNIX tail -oF
- Syslog : Nghe trên cổng 514 nổi tiếng cho các thông báo nhật ký hệ thống và phân tích định dạng theo RFC3164
- Redis : Đọc như máy chủ redis, sử dụng cả kênh redis và danh sách redis. Redis thường được sử dụng như 1 'broker' trong 1 mô hình Logstash tập trung, có hàng đợi các sự kiện Logstash từ các 'shippers' từ xa.
- Beats : Xử lý các sự kiện do beats gửi

### Filter

<img src="./Images/logstash-filter.png"/>

Filter là 1 thiết bị xử lý trung gian trong đường dẫn Logstash. Chúng ta có thể kết hợp các filter với các điều kiện để thực hiện 1 hành động trên 1 sự kiện nếu nó đáp ứng các tiêu chí nhất định. 1 số bộ lọc hữu ích:
- Grok : phân tích cú pháp và cấu trúc văn bản tùy ý - chỉnh sửa định dạng log từ client gửi về. Grok hiện là cách tốt nhất trong Logstash để phân tích cú pháp dữ liệu nhật ký không được cấu trúc thành một thứ có cấu trúc và có thể truy vấn được. Với 120 mẫu được tích hợp sẵn trong Logstash, nhiều khả năng chúng ta sẽ tìm thấy một mẫu đáp ứng nhu cầu của mình.
- Mutate : thực hiện các phép biến đổi chung trên các trường sự kiện. Bạn có thể đổi tên, xóa, thay thế và sửa đổi các trường trong sự kiện của mình.
- Drop : xóa hoàn toàn sự kiện, ví dụ: debug events
- Clone : tạo bản sao của sự kiện, có thể thêm hoặc xóa các trường.
- Geoip : thêm thông tin về vị trí địa lý của địa chỉ IP (cũng hiển thị biểu đồ tuyệt vời trong Kibana)

### Outputs

<img src="./Images/logstash-output.png"/>

Các output là pha cuối cùng của đường ống Logstash. Một sự kiện có thể đi qua nhiều output, nhưng một khi tất cả xử lý output đã hoàn tất, sự kiện đã hoàn tất việc thực thi của nó. Một số output thường được sử dụng bao gồm :
- Elasticsearch : gửi dữ liệu sự kiện tới Elasticsearch. Nếu chúng ta đang có kế hoạch để lưu dữ liệu trong một định dạng hiệu quả, thuận tiện, và dễ dàng truy vấn … Elasticsearch là con đường để đi.
- File : ghi dữ liệu sự kiện vào file trên bộ nhớ.
- Graphite : gửi dữ liệu sự kiện tới graphite, một công cụ nguồn mở phổ biến để lưu trữ và vẽ đồ thị số liệu.
- Statsd : gửi dữ liệu sự kiện đến statsd, một dịch vụ lắng nghe và thống kê.

## 1 số template

### Input

```
input {
  beats {
    port => 5044
    ssl => false
  }
}
```

### Filter

```
filter {
    grok {
      match => {
        "message" => [
          "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?:? %{SSH_INVALID_USER:message}"
        ]
      }
      patterns_dir => "/etc/logstash/patterns/sshd"
      named_captures_only => true
      remove_tag => ["_grokparsefailure"]
      break_on_match => true
      add_tag => [ "SSH", "SSH_INVALID_USER" ]
      add_field => { "event_type" => "SSH_INVALID_USER" }
      overwrite => "message"
    }
}

# Grok Filter for SSH Failed Password
filter{
    grok {
      match => {
        "message" => [
          "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?:? %{SSH_FAILED_PASSWORD:message}"
        ]
      }
      patterns_dir => "/etc/logstash/patterns/sshd"
      named_captures_only => true
      remove_tag => ["_grokparsefailure"]
      break_on_match => true
      add_tag => [ "SSH", "SSH_FAILED_PASSWORD" ]
      add_field => { "event_type" => "SSH_FAILED_PASSWORD" }
      overwrite => "message"
    }
}

filter {
# Grok Filter for SSH Password Accepted

    grok {
      match => {
        "message" => [
          "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?:? %{SSH_ACCEPTED_PASSWORD}"
        ]
      }
      patterns_dir => "/etc/logstash/patterns/sshd"
      named_captures_only => true
      remove_tag => ["_grokparsefailure"]
      break_on_match => true
      add_tag => [ "SSH", "SSH_ACCEPTED_PASSWORD" ]
      add_field => { "event_type" => "SSH_ACCEPTED_PASSWORD" }
    }
}
```

### Output

```
output {
     elasticsearch {
       hosts => ["localhost:9200"]
       sniffing => true
       index => "%{[@metadata][beat]}-%{+YYYY.MM.dd}"
     }
}
```

## Cài đặt Logstash

### Cài đặt Java 8 

Logstash yêu cầu phải cài đặt Java 8 để vận hành Logstash.
```
sudo yum install java-1.8.0-openjdk
```

Kiểm tra phiên bản
```
java --version 
```

### Cài đặt Logstash trên CentOS 7

Cài đặt thông tin Logstash Repository 
```
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
vi /etc/yum.repos.d/logstash.repo
```

```
name=Elastic repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
```

Tiến hành cài đặt gói Logstash
```
yum install logstash -y
```

Kiểm tra phiên bản của Logstash
```
/usr/share/logstash/bin/logstash --version
```

Khởi động Logstash và cấu hình startup service cho Logstash
```
systemctl enable logstash.service
systemctl start logstash.service
systemctl status logstash.service
```

## Kiểm tra cấu hình Logstash pipeline

- Đọc file **/var/log/demo.log** trên local ở server từ block cấu hình input{} sử dụng module "**file**"
- Nếu có nội dung được thêm vào thì đẩy vào file output : **/tmp/demo-logstash.log**
```
nano /etc/logstash/conf.d/demo-pipeline.conf
```

```
input {
    file {
        path => "/var/log/demo.log"
        start_position => "beginning"
    }
}

output {
    file {
        path => "/tmp/demo-logstash.log"
    }
}
```

Tạo file /var/log/demo.log
```
touch /var/log/demo.log
systemctl restart logstash
```

Thêm nội dung vào file /var/log/demo.log
```
echo "demo logstash pipeline works is ok - hoang" >> /var/log/demo.log
```

Kiểm tra xem ở file output đã thấy nội dung Logstash chưa
```
tail -f /tmp/demo-logstash.log
```