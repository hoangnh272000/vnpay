# Keepalived

## Introduction

Load balancing là 1 phương pháp phân phối 1 cluster của real servers, cung cấp 1 hoặc nhiều virtual services có sẵn. Khi thiết kế cấu trúc của load balancer, điều quan trọng là phải tính đến tính khả dụng của chính load balancer cũng như các servers sau nó.

Keepalived cung cấp frameworks cho cả load balancer và high availability. Load balancing framework dựa vào sự nổi tiếng và được dùng rộng rãi của Linux Virtual Server (IPVS), cung cấp Layer 4 load balancing. Keepalived triển khai 1 bộ health checkers để duy trì 1 cách thích ứng và quản lý nhóm load balanced server theo health. High availability đạt được nhờ Virtual Redundancy Routing Protocol (VRRP). VRRP là nền tảng cơ bản cho chuyển đổi dự phòng router. Keepalived triển khai 1 bộ hooks vào máy trạng thái hữu hạn VRRP cung cấp các giao thức tương tác low-level và high speed. Mỗi keepalived framwork có thể được sử dụng độc lập hoặc cùng nhau để cung cấp hạ tầng có khả năng chống chịu.

Keepalived cung cấp 2 chức năng chính:
- Health checking cho hệ thống LVS
- Triển khai VRRPv2 stack để xử lý chuyển đổi dự phòng load balancer.

<img src="./Images/keepalived.png"/>

## Keepalived Failover IP hoạt động như thế nào ?

Keepalived sẽ gom nhóm các máy chủ dịch vụ nào tham gia cụm HA, khởi tạo 1 Virtual Server đại diện cho 1 nhóm thiết bị đó với 1 **Virtual IP(VIP)** và 1 địa chỉ MAC vật lý của máy chủ dịch vụ đang giữ **Virtual IP(VIP)** đó. Vào mỗi thời điểm nhất định, chỉ có 1 server dịch vụ dùng địa chỉ MAC này tương ứng **Virtual IP(VIP)**. Khi có ARP request gửi tới **Virtual IP(VIP)** thì server dịch vụ đó sẽ trả về địa chỉ MAC này.

Các máy chủ dịch vụ sử dụng chung **VIP** phải liên lạc với nhau bằng địa chỉ *multicast 224.0.0.18* bằng giao thức VRRP. Các máy chủ sẽ có độ ưu tiên(priority) trong khoảng 1-254 và máy chủ nào có độ ưu tiên cao nhất sẽ thành **Master**, các máy chủ còn lại sẽ thành các **Slave/Backup** hoạt động ở chế độ chờ.

<img src="./Images/vip.png"/>

## Demo mô hình Keepalived IP Failover

Triển khai mô hình nginx và keepalived:
- 2 nginx load balancer (worker02, worker03)
- 2 nginx server (worker01, worker04)

### Cài đặt nginx

Cài các gói bằng lệnh:
```
sudo yum update -y
```

Cài đặt nginx:
```
sudo yum install epel-release
sudo yum install nginx -y
```

Khởi động nginx:
```
sudo systemctl start nginx
sudo systemctl status nginx
```

### Cài đặt và cấu hình KeepAlived trên 2 server

```
sudo yum install gcc kernel-headers kernel-devel
sudo yum install keepalived
```

File configuration  của KeepAlived ở `/etc/keepalived/keepalived.conf`. Cấu hình KeepAlived cơ bản nhất cho phép chia sẻ địa chỉ IP giữa 2 máy chủ. Worker02 sẽ là master, worker03 sẽ là backup.

<img src="./Images/keepalived-worker02.png"/>

<img src="./Images/keepalived-worker03.png"/>

Giải thích:
- `vrrp_instance` : Xác định 1 instance riêng lẻ của giao thức VRRP chạy trên 1 interface. Instance ở đây tên là VI_1
- `state` : Xác định state ban đầu mà instance sẽ bắt đầu.
- `interface` : Xác định interface mà VRRP chạy trên đó.
- `virtual_router_id` : Là định dạng duy nhất cho 1 VRRP instance và các IP address của nó trên network.
- `priority` : Là mức độ ưu tiên được quảng cáo. Nên sử dụng 255 cho master's priority
- `advert_int` : Chỉ định tần xuất gửi quảng cáo (thường là 1s)
- `authentication` : Chỉ định thông tin cần thiết cho servers tham gia VRRP để xác thực với nhau. 
- `virtual_ipaddress` : Xác định IP address (Có thể có nhiều) mà VRRP chịu trách nhiệm

Chỉnh sửa
```
nano /etc/keepalived/keepalived.conf
```

Đoạn code trên Node **Master**
```
! Configuration File for keepalived

global_defs {
   notification_email {
     acassen@firewall.loc
     failover@firewall.loc
     sysadmin@firewall.loc
   }
   notification_email_from Alexandre.Cassen@firewall.loc
   smtp_server 172.16.1.111
   smtp_connect_timeout 30
   router_id LVS_DEVEL
   vrrp_skip_check_adv_addr
   vrrp_strict
   vrrp_garp_interval 0
   vrrp_gna_interval 0
}

vrrp_instance VI_1 {
    state MASTER
    interface ens33
    virtual_router_id 51
    priority 255
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        192.168.27.135
    }
}

 track_script {
    check_nginx
  }
  authentication {
    auth_type AH
    auth_pass secret
  }
```

Đoạn code trên Node **Backup**
```
! Configuration File for keepalived

global_defs {
   notification_email {
     acassen@firewall.loc
     failover@firewall.loc
     sysadmin@firewall.loc
   }
   notification_email_from Alexandre.Cassen@firewall.loc
   smtp_server 172.16.1.111
   smtp_connect_timeout 30
   router_id LVS_DEVEL
   vrrp_skip_check_adv_addr
   vrrp_strict
   vrrp_garp_interval 0
   vrrp_gna_interval 0
}

vrrp_instance VI_1 {
    state BACKUP
    interface ens33
    virtual_router_id 51
    priority 254
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        192.168.27.135
    }
}

 track_script {
    check_nginx
  }
  authentication {
    auth_type AH
    auth_pass secret
  }
```

Sau khi chỉnh sửa 2 Node ta tiến hành khởi động lại Keepalived
```
systemctl start keepalived
systemctl enable keepalived
```

Kiểm tra trạng thái service keepalived
```
systemctl status keepalived
```

Kiểm tra IP
```
ip -br a
```

<img src="./Images/check-vip.png"/>